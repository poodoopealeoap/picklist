# picklist
A jQuery/Bootstrap3 plugin for making multi-select lists more user friendly.

# Installation
`npm install picklist`

# Documentation
Once install command is complete, documentation can be found at "./node_modules/picklist/Documentation.html"