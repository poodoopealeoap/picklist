destroy = () ->
	@element.OriginalParent.prepend(@element)
	$(@element).show()
	$("##{@id}, ##{@id}-modal, [data-picklist-launcher-for='#{@id}']").remove()
	delete window.Picklist.instances["#{@element.PicklistID}"]
	@element