class Picklist
	constructor: (element, options) ->
		@options = 
			selected			: options.selected 				? window.Picklist.defaults.selected
			height 				: options.height 				? window.Picklist.defaults.height
			striped 			: options.striped 				? window.Picklist.defaults.striped
			selectedTitle 		: options.selectedTitle 		? window.Picklist.defaults.selectedTitle
			availableTitle 		: options.availableTitle 		? window.Picklist.defaults.availableTitle
			color 				: options.color 				? window.Picklist.defaults.color
			modalize 			: options.modalize 				? window.Picklist.defaults.modalize
			modalTitle 			: options.modalTitle 			? window.Picklist.defaults.modalTitle
			launcherBtnContent 	: options.launcherBtnContent 	? window.Picklist.defaults.launcherBtnContent
			closeBtnContent 	: options.closeBtnContent 		? window.Picklist.defaults.closeBtnContent
			sortDropsBy 		: options.sortDropsBy 			? window.Picklist.defaults.sortDropsBy
			sortGroupsBy 		: options.sortGroupsBy 			? window.Picklist.defaults.sortGroupsBy
			defaultSort 		: options.defaultSort 			? window.Picklist.defaults.defaultSort
			beforeChange 		: options.beforeChange 			? window.Picklist.defaults.beforeChange
			afterChange 		: options.afterChange 			? window.Picklist.defaults.afterChange
			beforeLoad 			: options.beforeLoad 			? window.Picklist.defaults.beforeLoad
			afterLoad 			: options.afterLoad 			? window.Picklist.defaults.afterLoad
			search				: options.search				? window.Picklist.defaults.search
		
		@element = $(element)[0]
		@element.PicklistID = @element.id ? kula.guid()
		@element.OriginalParent = $(element).parent()
		@id = "picklist-#{@element.PicklistID}"

		@show 		= -> show.call this
		@hide 		= -> hide.call this
		@toggle 	= -> toggle.call this
		@reset 		= -> reset.call this
		@destroy 	= -> destroy.call this

		@options.beforeLoad.call @element, generateResponse.call(this)
		buildElements.call this
		setStyles.call this
		setListeners.call this
		setStartingState.call this
		@options.afterLoad.call @element, generateResponse.call(this)