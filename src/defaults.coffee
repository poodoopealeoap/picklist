window.Picklist =
	defaults:
		afterChange			: ->
		afterLoad			: ->
		availableTitle		: "Available"
		beforeChange		: ->
		beforeLoad			: ->
		closeBtnContent		: "Close"
		color				: "primary"
		defaultSort			: ["asc", "asc"]
		height				: "330px"
		launcherBtnContent	: "<i class=\"glyphicon glyphicon-th-list\"></i>"
		modalize			: false
		modalTitle			: ""
		selected			: []
		selectedTitle		: "Selected"
		sortGroupsBy		: "text"
		sortPicksBy			: "text"
		striped				: true
		search				: false
	instances: {}