setSelectedGroupVisibility = ->
    $("##{@id} .pl-selected [data-pick-group]").each ->
        totalChildren = $(@).children("[data-pick-value]").length
        hiddenChildren = 0
        $(@).children("[data-pick-value]").each ->
            if $(@).attr("style").indexOf("display: none") > -1 then hiddenChildren++
        if hiddenChildren is totalChildren
            $(@).hide()
        else
            $(@).show()