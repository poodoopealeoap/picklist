hideValuelessAvailableGroups = ->
    $("##{@id} .pl-available [data-pick-group]").each ->
        if $(@).children("[data-pick-value]").length is 0
            $(@).hide()
        else
            $(@).show()