setListeners = ->
	picklistObj = @

	setBadgeCounts = ->
		$("##{picklistObj.id} .pl-selected .badge").text $("##{picklistObj.id} .pl-selected [data-pick-value]").length
		$("##{picklistObj.id} .pl-available .badge").text $("##{picklistObj.id} .pl-available [data-pick-value]").length
	
	triggerSearch = ->
		if picklistObj.options.search
			$("##{picklistObj.id} .pl-search-available, ##{picklistObj.id} .pl-search-selected").change()

	sortAvailable = ->
		order = $("##{picklistObj.id} .pl-available .panel-heading.panel-title").attr "data-sort-order"
		kula.sortElements $("##{picklistObj.id} .pl-available [data-pick-group]"), picklistObj.options.sortGroupsBy, order
		kula.sortElements $("##{picklistObj.id} .pl-available [data-pick-value]"), picklistObj.options.sortPicksBy, order

	sortSelected = ->
		order = $("##{picklistObj.id} .pl-selected .panel-heading.panel-title").attr "data-sort-order"
		kula.sortElements $("##{picklistObj.id} .pl-selected [data-pick-group]"), picklistObj.options.sortGroupsBy, order
		kula.sortElements $("##{picklistObj.id} .pl-selected [data-pick-value]"), picklistObj.options.sortPicksBy, order
	
	updateLauncher = ->
		$("[data-picklist-launcher-for='#{picklistObj.id}'] > i.form-control").html ( ->
			res = generateResponse.call picklistObj
			if res.selected.length is 0 
				return "&nbsp;"
			return ("<div>#{pick.text}</div>" for pick in res.selected)
		)()
		kula.sortElements $("[data-picklist-launcher-for='#{picklistObj.id}'] > i.form-control").children(), "text", "asc"

	updateSelectValue = ->
		$("##{picklistObj.id} select").val ($(pick).attr "data-pick-value" for pick in $("##{picklistObj.id} .pl-selected [data-pick-value]"))

	#Sort Toggle Action
	$("##{@id} .panel-heading.panel-title")
		.attr "data-sort-order", "asc"
		.click ->
			glyph = $(@).children().children().children('.glyphicon')
			if $(@).attr("data-sort-order") is "asc"
				$ @
					.attr "data-sort-order", "desc"
				glyph
					.removeClass "glyphicon-sort-by-attributes"
					.addClass "glyphicon-sort-by-attributes-alt"
				if $(@).siblings(".panel-body").children("[data-pick-group]").length > 0
					kula.sortElements $(@).siblings(".panel-body").children("[data-pick-group]"), picklistObj.options.sortGroupsBy, "desc"
					kula.sortElements $(@).siblings(".panel-body").children("[data-pick-group]").children("[data-pick-value]"), picklistObj.options.sortPicksBy, "desc"
				else
					kula.sortElements $(@).siblings(".panel-body").children("[data-pick-value]"), picklistObj.options.sortPicksBy, "desc"
			else
				$ @
					.attr "data-sort-order", "asc"
				glyph
					.removeClass "glyphicon-sort-by-attributes-alt"
					.addClass "glyphicon-sort-by-attributes"
				if $(@).siblings(".panel-body").children("[data-pick-group]").length > 0
					kula.sortElements $(@).siblings(".panel-body").children("[data-pick-group]"), picklistObj.options.sortGroupsBy, "asc"
					kula.sortElements $(@).siblings(".panel-body").children("[data-pick-group]").children("[data-pick-value]"), picklistObj.options.sortPicksBy, "asc"
				else
					kula.sortElements $(@).siblings(".panel-body").children("[data-pick-value]"), picklistObj.options.sortPicksBy, "asc"
			setStyles.call picklistObj, true
		

	#Pick Swap Action
	$("##{@id} [data-pick-value]").click ->
		picklistObj.options.beforeChange.call picklistObj.element, generateResponse.call(picklistObj)
		
		if $("##{picklistObj.id} [data-pick-group]").length > 0
			if $(@).parent().parent().parent().hasClass "pl-selected"
				$("##{picklistObj.id} .pl-available [data-pick-group='#{$(@).parent().attr "data-pick-group" }']").append $(@)
			else
				$("##{picklistObj.id} .pl-selected [data-pick-group='#{$(@).parent().attr "data-pick-group" }']").append $(@)
		else
			if $(this).parent().parent().hasClass "pl-selected"
				$("##{picklistObj.id} .pl-available .panel-body").append $(@)
			else
				$("##{picklistObj.id} .pl-selected .panel-body").append $(@)
		
		setBadgeCounts()
		updateSelectValue()
		updateLauncher()
		if $(@).parent().parent().parent().hasClass "pl-available" then sortAvailable() else sortSelected()
		setStyles.call picklistObj, true
		triggerSearch()
		
		setSelectedGroupVisibility.call picklistObj
		picklistObj.options.afterChange.call picklistObj.element, generateResponse.call(picklistObj)


	$("##{@id} [data-pick-group] h6").click ->
		picklistObj.options.beforeChange.call picklistObj.element, generateResponse.call(picklistObj)

		kula.sortElements $("[data-picklist-launcher-for='#{picklistObj.id}'] > i.form-control").children(), "text", "asc"

		if $(@).parent().parent().parent().hasClass "pl-selected"
			$(@).siblings().each ->
				$("##{picklistObj.id} .pl-available [data-pick-group='#{$(@).parent().attr "data-pick-group"}']").append $(@)
			sortAvailable()
		else
			$(@).siblings().each ->
				$("##{picklistObj.id} .pl-selected [data-pick-group='#{$(@).parent().attr "data-pick-group"}']").append $(@)
			sortSelected()
		
		setBadgeCounts()
		updateSelectValue()
		updateLauncher()
		setStyles.call picklistObj, true
		triggerSearch()

		setSelectedGroupVisibility.call picklistObj
		picklistObj.options.afterChange.call picklistObj.element, generateResponse.call(picklistObj)


	$("[data-picklist-launcher-for='#{@id}'] > *").click ->
		$("##{picklistObj.id}-modal").modal "show"


	["available", "selected"].forEach (item) ->
		$("##{picklistObj.id} .pl-search-#{item}").on "keyup change", ->
			query = @value.toLowerCase()
			$("##{picklistObj.id} .pl-#{item} [data-pick-value]").each ->
				if $(@).text().toLowerCase().indexOf(query) is -1 and query.length > 0
					$(@).hide()
				else
					$(@).show()
			$("##{picklistObj.id} .pl-#{item} [data-pick-group]").each ->
				totalChildren = $(@).children("[data-pick-value]").length
				hiddenChildren = 0
				$(@).children("[data-pick-value]").each ->
					if $(@).attr("style").indexOf("display: none") > -1 then hiddenChildren++
				if hiddenChildren is totalChildren
					$(@).hide()
				else
					$(@).show()