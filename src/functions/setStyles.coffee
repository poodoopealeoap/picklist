setStyles = (reset = false) ->
	stripeColor = kula.getStyle "table.table.table-striped tbody tr", "background-color"
	panelBorderRadius = kula.getStyle ".panel.panel-default", "border-bottom-left-radius"
	panelColor = kula.getStyle ".panel.panel-#{@options.color} .panel-heading.panel-title", "background-color"

	$ "##{@id} > *"
		.css
			"user-select"	: "none"
			"cursor"		: "default"
	
	$ "##{@id} .panel"
		.css
			"margin-bottom": "0px"
	
	$ "##{@id} .panel-heading.panel-title"
		.css
			"font-size"		: "1em"
			"transition"	: "background-color 0.4s, box-shadow 0.3s"
			"cursor"		: "pointer"

	$ "##{@id} .panel-body"
		.css
			"padding"		: "0px"
			"overflow-x"	: "none"
			"overflow-y"	: "auto"
			"border-radius"	: "0px 0px #{panelBorderRadius} #{panelBorderRadius}"

	$ "##{@id} .panel-footer"
		.css
			"border-top" 	: "1px solid #{kula.darken(stripeColor, 10)}"
			"padding"		: "3px 15px 6px"

	$ "##{@id} .glyphicon-transfer"
		.css
			"font-size": "1.8em"
			"padding-bottom" : if window.innerWidth < 768 then 15 else Math.round parseFloat(@options.height) / 2

	$ "##{@id} [data-pick-group] h6"
		.css
			"margin"			: "0px"
			"padding"			: "8px 15px"
			"background-color"	: kula.darken(stripeColor, 10)
			"border-top"		: "1px solid #{kula.darken(stripeColor, 15)}"
			"border-bottom"		: "1px solid #{kula.darken(stripeColor, 15)}"
			"cursor"			: "pointer"

	$ "##{@id} [data-pick-value]"
		.css
			"margin"			: "0px"
			"padding"			: "5px 15px"
			"cursor"			: "pointer"
			"background-color"	: $("body").css "background-color"

	if @options.striped
		if $("##{@id} [data-pick-group]").length > 0
			$ "##{@id} [data-pick-value]:nth-child(even)"
				.css
					"background-color": kula.darken stripeColor, 1.5
		else
			$ "##{@id} [data-pick-value]:nth-child(odd)"
				.css
					"background-color": kula.darken stripeColor, 1.5
	
	$ "##{@id} [data-pick-group] [data-pick-value]"
		.css
			"padding": "5px 30px"

	$ "##{@id} .badge"
		.css
			"margin-left"	: "5px"
			"margin-top"	: "-2px"

	$ "[data-picklist-launcher-for='#{@id}'] .input-group-btn"
		.css
			"vertical-align": "top"

	$ "[data-picklist-launcher-for='#{@id}'] i.form-control"
		.css
			"height"	: "auto"
			"max-height": "100px"
			"overflow-y": "auto"
			"cursor"	: "pointer"

	$ "[data-picklist-launcher-for='##{@id}'] i.form-control > div"
		.css
			"user-select": "none"

	xs = ->
		if window.innerWidth < 768
			$ "##{@id} .glyphicon-transfer"
				.css
					"transform"		: "rotate(90deg)"
					"padding-top"	: "15px"
					"padding-bottom": "15px"

			$ "##{@id} .panel-body"
				.css
					"height": "230px"

			$ "##{@id} > .col-sm-2, ##{@id} > .col-sm-5"
				.css
					"width": "100%"
		else
			$ "##{@id} .panel-body"
				.css
					"height": @options.height
			transferPadding = Math.round parseFloat(@options.height) / 2
			$ "##{@id} .glyphicon-transfer"
				.css
					"transform"		: "rotateX(180deg)"
					"padding-bottom": transferPadding
			$ "##{@id} > .col-sm-2"
				.css
					"width": "8.66666667%"
			$ "##{@id} > .col-sm-5"
				.css
					"width": "45.66666667%"
	if not reset
		$ "##{@id} .panel-heading.panel-title"
			.mouseover ->
				$ this
					.css
						"box-shadow": "0 0 40px 40px rgba(0,0,0,0.1) inset"
			.mouseout ->
				$ this
					.css
						'box-shadow': "0 0 40px 40px rgba(0,0,0,0) inset"

		picklistObj = this
		$ window
			.on "resize", ->
				xs.call picklistObj
		$ window
			.on "load", ->
				xs.call picklistObj

	xs.call this