buildElements = ->
	$(@element).hide().prop "multiple", true

	optgroups = $(@element).children "optgroup"
	
	if optgroups.length > 0
		groups = (for group, i in optgroups
			"<div data-pick-group='#{i}' data-optgroup-label='#{group.label}'>
				<h6>#{group.label}</h6>
			</div>"
		).join ""
		values = (for group, i in optgroups
			"<div data-pick-group='#{i}' data-optgroup-label='#{group.label}'>
				<h6>#{group.label}</h6>
				#{
					(for option in $(group).children()
						"<div data-pick-value='#{option.value}'>#{option.innerText}</div>"
					).join ""
				}
			</div>"
		).join ""
	else
		values = ("<div data-pick-value='#{option.value}'>#{option.innerText}</div>" for option in $(@element).children()).join ""
	
	
	pickElements = 
		"<div class='row picklist' id='#{@id}'>
			<div class='col-sm-5'>
				<div class='panel panel-#{@options.color} pl-selected'>
					<div class='panel-heading panel-title'>
						<div class='row'>
							<div class='col-xs-10'>
								#{@options.selectedTitle}<span class='badge'></span>
							</div>
							<div class='col-xs-2 text-right'>
								<i class='glyphicon glyphicon-sort-by-attributes'></i>
							</div>
						</div>
					</div>
					<div class='panel-body'>#{ (if groups is undefined then "" else groups) }</div>
					#{
						if @options.search then "<div class='panel-footer'><input type='search' class='pl-search-selected form-control input-sm' autocomplete='off' placeholder='Search...'></div>" else ""
					}
				</div>
			</div>
			<div class='col-sm-2 text-center'>
				<i class='glyphicon glyphicon-transfer'></i>
			</div>
			<div class='col-sm-5'>
				<div class='panel panel-#{@options.color} pl-available'>
					<div class='panel-heading panel-title'>
						<div class='row'>
							<div class='col-xs-10'>
								#{@options.availableTitle}<span class='badge'></span>
							</div>
							<div class='col-xs-2 text-right'>
								<i class='glyphicon glyphicon-sort-by-attributes'></i>
							</div>
						</div>
					</div>
					<div class='panel-body'>
						#{values}
					</div>
					#{
						if @options.search then "<div class='panel-footer'><input type='search' class='pl-search-available form-control input-sm' autocomplete='off' placeholder='Search...'></div>" else ""
					}
				</div>
			</div>"
	if this.options.modalize
		$ @element
			.before(
				"<div class='input-group' data-picklist-launcher-for='#{@id}'>
					<i class='form-control'>&nbsp;</i>
					<span class='input-group-btn'>
						<button type='button' class='btn btn-default' data-toggle='modal' data-target='#{@id}-modal'>#{@options.launcherBtnContent}</button>
					</span>
				</div>"
			)
		$ @element
			.after(
				"<div class='modal fade' id='#{@id}-modal' role='dialog'>
					<div class='modal-dialog modal-lg'>
						<div class='modal-content'>
							<div class='modal-header'>
								<button type='button' class='close' data-dismiss='modal' aria-hidden='true'>x</button>
								<h4 class='modal-title'>#{@options.modalTitle}</h4>
							</div>
							<div class='modal-body'>
								#{pickElements}
							</div>
							<div class='modal-footer'>
								<button type='button' class='btn btn-default' data-dismiss='modal'>#{@options.closeBtnContent}</button>
							</div>
						</div>
					</div>
				</div>"
			)
	else
		$(@element).after(pickElements)
	
	$("##{@id}").prepend(@element)