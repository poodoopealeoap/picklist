generateResponse = ->
	response = 
		selected: []
	
	values = $("##{@id} > select").val()
	if values?
		response.selected = (value: val, text: $("##{@id} [data-pick-value='#{val}']").text() for val in values)

	response