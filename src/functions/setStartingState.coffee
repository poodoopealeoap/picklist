setStartingState = ->
	$("##{@id} .pl-selected .badge").text "0"
	$("##{@id} .pl-available .badge").text $("##{@id} .pl-available [data-pick-value]").length

	# pre-select options.selected items
	$("##{@id} .pl-available [data-pick-value='#{selection}']").click() for selection in @options.selected

	# set starting sort
	if @options.defaultSort[0] is "desc"
		$("##{@id} .pl-selected .panel-heading.panel-title").click()
	else
		$("##{@id} .pl-selected .panel-heading.panel-title").click().click()
	
	if @options.defaultSort[1] is "desc"
		$("##{@id} .pl-available .panel-heading.panel-title").click()
	else
		$("##{@id} .pl-available .panel-heading.panel-title").click().click()


	setSelectedGroupVisibility.call @
	hideValuelessAvailableGroups.call @