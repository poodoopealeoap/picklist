init = (options = {}) ->
	if this.PicklistID?
		window.Picklist.instances[this.PicklistID]
	else
		instance = new Picklist this, options
		window.Picklist.instances[instance.element.PicklistID] = instance
		instance

$.fn.extend
	picklist: init