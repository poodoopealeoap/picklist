function scrollToSection(e) {
	e.preventDefault();
	$('html,body').animate({
		scrollTop: $('#' + this.href.split('#')[1]).offset().top
	}, 200);
}
$('nav a').click(scrollToSection);
$('.innerlink').click(scrollToSection);
var points = [];
var ranges = [];
$('.container').each(function() {
	points.push($(this).offset().top);
});
for (var i = 0; i < points.length - 1; i++) {
	ranges.push([points[i], points[i + 1]]);
}
ranges.push([
	points[points.length - 1],
	points[points.length - 1] + parseFloat($('.container').last().css('height'))
]);
function navSpy() {
	for (var i = 0; i < ranges.length; i++) {
		var min = ranges[i][0] - (window.innerHeight * 0.3);
		var max = ranges[i][1] - (window.innerHeight * 0.3);
		min = min < 0 ? 0 : min;
		if (window.scrollY >= min && window.scrollY <= max) {
			if (!$($('nav a')[i]).hasClass('bg-primary')) {
				$('nav a').removeClass('bg-primary');
				$($('nav a')[i]).addClass('bg-primary');
			}
		}
	}
}
$(window).scroll(navSpy);
$(document).ready(navSpy);


$('select').kuntree('US');
$('#ex1').picklist({search:true, modalize: true});
$('#ex2').picklist({
	color: 'warning',
	afterChange: function() {
		alert($(this).val());
	}
});