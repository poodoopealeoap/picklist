'use strict';

var gulp = require('gulp'),
	concat = require('gulp-concat'),
	coffee = require('gulp-coffee'),
	rename = require('gulp-rename'),
	uglify = require('gulp-uglify'),
	jade = require('gulp-jade'),
	del = require('del');

gulp.task('compile', function() {
	return gulp.src('src/**/*.coffee')
		.pipe(concat('picklist.js'))
		.pipe(coffee())
		.pipe(gulp.dest('dist'));
});

gulp.task('min', ['compile'], function() {
	return gulp.src('dist/picklist.js')
		.pipe(uglify({ mangle: false }))
		.pipe(rename('picklist.min.js'))
		.pipe(gulp.dest('dist'));
});

gulp.task('docs', function() {
	return gulp.src('src/docs/Documentation.jade')
		.pipe(jade())
		.pipe(gulp.dest('./'));
});

gulp.task('watch', ['min', 'docs'], function() {
	gulp.watch('src/**/*.coffee', ['min']);
	gulp.watch('src/**/*.jade', ['docs']);
});

gulp.task('default', ['min']);